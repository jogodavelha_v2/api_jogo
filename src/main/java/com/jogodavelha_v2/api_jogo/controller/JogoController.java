package com.jogodavelha_v2.api_jogo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jogodavelha_v2.api_jogo.model.Jogada;
import com.jogodavelha_v2.api_jogo.model.Jogador;
import com.jogodavelha_v2.api_jogo.model.Jogo;
import com.jogodavelha_v2.api_jogo.model.Placar;
import com.jogodavelha_v2.api_jogo.model.Rodada;
import com.jogodavelha_v2.api_jogo.service.JogadorService;
import com.jogodavelha_v2.api_jogo.service.Message;



@Controller
@CrossOrigin
public class JogoController {
	
	@Autowired
	JogadorService jogadorService;
	
	Jogador[] jogadores = new Jogador[2];
	Rodada rodada = new Rodada("Jogador 1", "Jogador 2");
	
	@RequestMapping(path = "/jogo/criar", method = RequestMethod.GET)
	public @ResponseBody
	Jogo criarJogo(@RequestParam int idJogador01, int idJogador02, int idRodada) throws Exception {
		
		Jogador jogador01 = new Jogador();
		//jogador01 = jogadorService.buscarJogador(idJogador01);
		Message.sendMessage("Criou jogador 01");
		
		Jogador jogador02 = new Jogador();
		Message.sendMessage("Criou jogador 02");
		//jogador02 = jogadorService.buscarJogador(idJogador02);
		
		jogadores[0] = jogador01;
		jogadores[1] = jogador02;
		
		Jogo jogo = new Jogo();
		Message.sendMessage("Criou novo jogo");
		jogo.setJogadores(jogadores);
		jogo.setIdRodada(idRodada);
		
		return jogo;
	}
	
	@RequestMapping(path = "/jogo/jogar", method = RequestMethod.POST)
    public @ResponseBody
    Placar jogar(@RequestBody Jogada jogada) throws Exception {
        rodada.jogar(jogada.x, jogada.y);
        Message.sendMessage("Nova jogada: "+jogada.x+"-"+jogada.y);
        return rodada.getPlacar();
    }
	
}
