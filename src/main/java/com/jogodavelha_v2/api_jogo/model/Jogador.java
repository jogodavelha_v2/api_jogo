package com.jogodavelha_v2.api_jogo.model;


public class Jogador {
	
	public Jogador() {
		
	}
	
	public int id;

	public String nome;

	public int qtdeVitorias;

	public int qtdeDerrotas;

	public int qtdeEmpates;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getQtdeVitorias() {
		return qtdeVitorias;
	}
	public void setQtdeVitorias(int qtdeVitorias) {
		this.qtdeVitorias = qtdeVitorias;
	}
	public int getQtdeDerrotas() {
		return qtdeDerrotas;
	}
	public void setQtdeDerrotas(int qtdeDerrotas) {
		this.qtdeDerrotas = qtdeDerrotas;
	}
	public int getQtdeEmpates() {
		return qtdeEmpates;
	}
	public void setQtdeEmpates(int qtdeEmpates) {
		this.qtdeEmpates = qtdeEmpates;
	}
	public int getPontos() {
		return qtdeVitorias;
	}
	public void incrementarPontos(){
		qtdeVitorias++;
	}


}
